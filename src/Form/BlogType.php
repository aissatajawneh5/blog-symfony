<?php   

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\BlogEntity;


class BlogType extends AbstractType {

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add("titre", TextType::class)
        ->add("sujet", TextType::class,[
            "label" => "Sujet"
        ])
        ->add("text", TextType::class, [
            "label" => "Text"
        ])
        ->add("auteur", TextType::class, [
            "label" => "Auteur"
        ])
        ->add("date", TextType::class);/*, DateType::class, [
            "label" => "Date"
        ]);*/
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => BlogEntity::class 
        ]);
    }


}