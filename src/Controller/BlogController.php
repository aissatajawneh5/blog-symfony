<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BlogRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BlogEntity;
use App\Form\BlogType;


class BlogController extends AbstractController{
    /**
     * @Route("/", name = "blog")
     */
    public function page1(Request $request, BlogRepository $repo){
        
        $article = new BlogEntity();
            $form = $this->createForm(BlogType::class, $article);

        $form->handleRequest($request);
        
        $repo->findAll();

        if($form->isSubmitted() && $form->isValid()) {
           $repo->add($article);
           return  $this -> redirectToRoute('show_all');
            dump($article);
        }
      return  $this->render("blog.html.twig", [
        "form" => $form->createView(),
        "art" => $article,
      
      ]);
    }


    /**
     * @Route("/carroussel", name = "show_carroussel")
     */
    public function showCarroussel(Request $request){

      return $this->render("team.html.twig");
    }


 /**
     * @Route("/show-all", name = "show_all")
     */
    public function index(BlogRepository $repo){
      
      $art = $repo->findAll();

     return $this->render("show-all.html.twig", [
       
         "art" =>$art 
     ]);
 }


   /**
     * @Route ("/show-one/{id}", name="show_one")
     */
    public function oneArt(BlogRepository $repo, int $id){
      //On se sert de l'argument de la route pour aller chercher
      //un vélo spécifique avec la méthode find du repository
      
      return $this->render("show-one.html.twig",[
        "art" => $repo->find($id) 
      ]);
  }


   /**
     * @Route("/delete/{id}", name = "delete")
     */
    public function Supprimer(int $id, BlogRepository $repo)
    { 
        $repo->remove($repo->find($id));
    
        return $this->redirectToRoute('show_all',[
            // 'delete' => $delete
        ]);
    }

    /**
     * @Route("/modifier/{id}", name = "modifier")
     */
    public function Modifier(BlogRepository $repo, Request $query, int $id)
    {
      $article = $repo->find($id);

      $form = $this->createForm(BlogType::class, $article);
      $form->handleRequest($query);

      if($form->isSubmitted() && $form->isValid()) {
        /**
         * Si le formulaire est submit et valide, alors on déclenche
         * la méthode add de notre repository pour faire persister
         * l'instance de Bicycle liée à notre formulaire
         */
        $repo->update($article);
        return  $this -> redirectToRoute('show_all');

        dump($article);
    }
    
    return $this->render("modifier.html.twig", [
        "form" => $form->createView(),
        "art" => $article
   ]);
    }

}   


//je dois crée un fichier pour faire en sorte d'afficher un seul article quand on clique dessus,
//faire en sorte de supprimer un article ou le modifier.
//