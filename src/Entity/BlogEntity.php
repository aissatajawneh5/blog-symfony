<?php

namespace App\Entity;

class BlogEntity {
    public $id;
    public $titre;
    public $sujet;
    public $auteur;
    public $text;
    public $date;
}