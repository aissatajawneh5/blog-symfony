<?php

namespace App\Repository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\BlogEntity;


class BlogRepository extends AbstractController
{
    /**
     * @return Article []
     */
    public function findAll()
    {
        $articles = [];
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM Articles");
        $query->execute();

        $results = $query->fetchAll();
        foreach ($results as $line) {
            //On appel la méthode sqlToBike pour récupérer une
            //instance de bike correspondant à la ligne de résultat
            //actuelle et on push cette instance dans le tableau
             $articles[] = $this->sqlToBlog($line);
             //array_push($bikes, $bike);
         }

       // $articles[] = $this->sqlToBlog($line);

        return $articles;
    }

    public function find(int $id): ?BlogEntity {
        dump($id);
        $connection = ConnectionUtil::getConnection();
        /**
         * On prépare la requête et on assigne les value aux
         * placeholders
         */
        $query = $connection->prepare("SELECT * FROM Articles WHERE idArticles = :id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
       // dump($query->fetch());
        if($line = $query->fetch()) {
            //On return le Bicycle en se servant de la méthode si 
            //résultat il y a
            return $this->sqlToBlog($line);

        }
        //On return null si on est pas passé par le if
        return null;
    }

    public function add(BlogEntity $article){

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("INSERT INTO Articles (titre,sujet,date,text,auteur) VALUES (:titre,:sujet,:date,:text,:auteur)");
        /**
         * Pour assigner des valeurs aux placeholders de notre requête
         * on utilise la méthode bindValue qui attend en premier le
         * nom du placeholder et en deuxième la valeur à lui assigner,
         * et optionnelement en troisième le type de donnée attendu
         * par la colonne de la table SQL
         */
        $query->bindValue(":titre",$article->titre, \PDO::PARAM_STR);
        $query->bindValue(":sujet", $article->sujet, \PDO::PARAM_STR);
        $query->bindValue(":date", $article->date, \PDO::PARAM_STR);
        $query->bindValue(":text", $article->text, \PDO::PARAM_STR);
        $query->bindValue(":auteur", $article->auteur, \PDO::PARAM_STR);

        $query->execute();

        //$article->id = $connection->lastInsertId();
    }

     /**
     * Méthode qui va mettre à jour les valeurs d'un Bicycle donné.
     * L'argument attendu est donc un Bicycle avec un id pour pouvoir
     * le modifier
     */
    public function update(BlogEntity $article): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("UPDATE Articles SET titre=:titre, sujet=:sujet, date=:date, text=:text, auteur=:auteur WHERE idArticles=:id");
        $query->bindValue(":titre", $article->titre, \PDO::PARAM_STR);
        $query->bindValue(":sujet", $article->sujet, \PDO::PARAM_STR);
        $query->bindValue(":date", $article->date, \PDO::PARAM_STR);
        $query->bindValue(":text", $article->text, \PDO::PARAM_STR);
        $query->bindValue(":auteur",$article->auteur, \PDO::PARAM_STR);
        $query->bindValue(":id",$article->id, \PDO::PARAM_INT);

        $query->execute();

    }

    public function remove(BlogEntity $article): void {
        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("DELETE FROM Articles WHERE idArticles=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }




    private function sqlToBlog(array $line): BlogEntity
    {
        //On crée l'instance
        $art = new BlogEntity();
        //On assigne les valeurs de la ligne de résultat aux 
        //différentes propriétés de notre classe (c'est ici qu'on va
        //convertir les données qui doivent l'être etc.)
        $art->id = intval($line["idArticles"]);
        $art->titre = $line["titre"];
        $art->sujet = $line["sujet"];
        $art->auteur = $line["auteur"];
        $art->date = strval($line["date"]);
        $art->text = $line["text"];

        //On renvoie l'instance 
        return $art;
    }
}

